//
//  Mundo.h
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#pragma once

#include "PhysicsGameEntity.h"
#include "Lata.h"
#include "Bola.h"


class Mundo: public PhysicsGameEntity {
    
public:
    bool init();
    ~Mundo();
    static Mundo* getInstance();
    static void detroyInstance();
    
    virtual void preloadResources();
    virtual Node* getNode();
    void update(float delta);
   
    b2World* getPhysicsWorld();
    Vector<Lata*> getLatas();
    
private:
    
    CREATE_FUNC(Mundo);
    static Mundo *m_instance;
   
    Vector<Lata*> m_latas;
    Bola *m_bola;
    
    EventListenerTouchOneByOne *m_listener;
    
    b2World *m_world;
    b2Fixture *m_fixture_goal;
};

class GoalContactListener : public b2ContactListener {
    
public:
    
    GoalContactListener(b2Fixture *fixture);
    
    // Se produce un contacto entre dos cuerpos
    virtual void BeginContact(b2Contact* contact);
    
    // El contacto entre los cuerpos ha finalizado
    virtual void EndContact(b2Contact* contact);
    
private:
    b2Fixture *m_fixture;
};

