//
//  MarioSc.cpp
//  Test01
//
//  Created by Fidel Aznar on 13/1/15.
//
//

#include "Game.h"
#include "../Util/Singleton.h"

Mundo* Mundo::m_instance = NULL;

bool Game::init(){
    
    SimpleGame::init();
    
    m_mundo = Mundo::getInstance();
    addGameEntity(m_mundo);
    
    m_gui = GUI::create();
    m_gui->retain();
    addGameEntity(m_gui);

    this->preloadResources();
    this->start();
    
    return true;
}

Game::~Game(){
    m_gui->release();
    Mundo::detroyInstance();
}

void Game::preloadResources(){
    SpriteFrameCache::getInstance()->addSpriteFramesWithFile("sprites.plist");
    preloadEachGameEntity();
    
}

void Game::start(){
    addEachGameEntityNodeTo(this);
}

void Game::updateEachFrame(float delta){
    updateEachGameEntityWithDelta(delta);
}

