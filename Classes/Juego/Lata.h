//
//  Npc.h
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#pragma once

#include <Box2D/Box2D.h>

#include "../Engine2D/PhysicsGameEntity.h"

class Lata: public PhysicsGameEntity{
    
public:
    
    bool init();
    
    Node* getNode();
    
    CREATE_FUNC(Lata);
    
private:
    
    Sprite *m_sprite;
};
