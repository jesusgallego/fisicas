//
//  Bola.cpp
//  VideojuegosCocos
//
//  Created by Miguel Angel Lozano on 14/1/15.
//
//


#include "Bola.h"
#include "Mundo.h"
#include "Box2D/Box2D.h"

bool Bola::init(){
    PhysicsGameEntity::init();
    return true;
}

Node* Bola::getNode(){
    
    if(m_node==NULL){

        // Crea nodo gráfico
        m_bolaSprite = Sprite::createWithSpriteFrameName("bola.png");
        m_node = m_bolaSprite;
        m_node->setLocalZOrder(1);

        // Crea cuerpo físico
        b2World *world = Mundo::getInstance()->getPhysicsWorld();
        
        b2BodyDef bodyDef;
        bodyDef.type = b2BodyType::b2_dynamicBody;
        
        b2FixtureDef fixtureDef;
        b2CircleShape shape;
        shape.m_radius = 0.5;
        fixtureDef.shape = &shape;
        fixtureDef.density = 1.0;
        fixtureDef.restitution = 0.5;
        
        m_body = world->CreateBody(&bodyDef);
        m_body->CreateFixture(&fixtureDef);
    }
    
    return m_node;
    
}
