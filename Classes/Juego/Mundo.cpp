//
//  Mundo.cpp
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#include "Mundo.h"
#include "iostream"

#define NUM_LATAS   3
#define NUM_PUNTOS  5


bool Mundo::init(){
    GameEntity::init();
    
    // Inicializa nodos gráficos del mundo
    m_bola = Bola::create();
    m_bola->retain();
    
    for(int i=0;i<NUM_LATAS;i++) {
        m_latas.pushBack(Lata::create());
    }
    
    // Inicializa mundo de físicas
    m_world = new b2World(b2Vec2(0, -10));
    
    return true;
}

void Mundo::detroyInstance() {
    if(m_instance!=NULL) {
        m_instance->release();
        m_instance = NULL;
    }
}

Mundo* Mundo::getInstance(){
    
    if(Mundo::m_instance==NULL) {
        Mundo::m_instance = Mundo::create();
        //Hago un retain hasta el final de los tiempos, soy un singleton
        m_instance->retain();
    }
    
    return Mundo::m_instance;
}

Mundo::~Mundo(){
    CCLOG("Mundo es Singleton, no debería morir hasta el final. Cuidado...");
    m_latas.clear();
    m_bola->release();
    if(m_instance!=NULL) {
        m_instance->release();
        m_instance = NULL;
    }
}

void Mundo::preloadResources(){

}

Node* Mundo::getNode(){
    
    Size size = Director::getInstance()->getWinSize();
    
    Node *node = Node::create();
    
    //Añado las latas en las posiciones que corresponda
    int altura = 150;
    for (auto lata: this->m_latas){
        node->addChild(lata->getNode());
        lata->setTransform(Vec2(240, altura), 0);
        
        altura += 75;
    }
    
    //Añado la bola en su posición inicial
    node->addChild(m_bola->getNode());
    m_bola->setTransform(Vec2(50, 100), 0);
    
    
    
    
    // Crea límites físicos del escenario
    
    // Descomenta para el apartado (c)
     
     b2BodyDef bodyDef;
     bodyDef.position.Set(0, 0);
     bodyDef.type = b2BodyType::b2_staticBody;
     
     m_body = m_world->CreateBody(&bodyDef);
     
     b2EdgeShape limitesShape;
     b2FixtureDef fixtureDef;
     fixtureDef.shape = &limitesShape;
     
     limitesShape.Set(b2Vec2(0.0f / PTM_RATIO, 0.0f / PTM_RATIO),
     b2Vec2(size.width / PTM_RATIO, 0.0f / PTM_RATIO));
     m_body->CreateFixture(&fixtureDef);
     
     limitesShape.Set(b2Vec2(size.width / PTM_RATIO, 0.0f / PTM_RATIO),
     b2Vec2(size.width / PTM_RATIO, size.height / PTM_RATIO));
     m_body->CreateFixture(&fixtureDef);
     
     limitesShape.Set(b2Vec2(size.width / PTM_RATIO, size.height / PTM_RATIO),
     b2Vec2(0.0f / PTM_RATIO, size.height / PTM_RATIO));
     m_body->CreateFixture(&fixtureDef);
     
     limitesShape.Set(b2Vec2(0.0f / PTM_RATIO, size.height / PTM_RATIO),
     b2Vec2(0.0f / PTM_RATIO, 0.0f / PTM_RATIO));
     m_body->CreateFixture(&fixtureDef);
    
    
    
    
    // Crea suelo mediante una cadena
    
    // Descomenta para el apartado (d)
     
     b2Vec2 v[NUM_PUNTOS];
     v[0].Set(0.0f / PTM_RATIO, 0.0f / PTM_RATIO);
     v[1].Set(120.0f / PTM_RATIO, 5.0f / PTM_RATIO);
     v[2].Set(240.0f / PTM_RATIO, 20.0f / PTM_RATIO);
     v[3].Set(320.0f / PTM_RATIO, 80.0f / PTM_RATIO);
     v[4].Set(480.0f / PTM_RATIO, 0.0f / PTM_RATIO);
     
     b2ChainShape chain;
     chain.CreateChain(v, 5);
     
     m_body->CreateFixture(&chain, 0);
     
     // Dibujamos escenario estático
    
     DrawNode *drawNode = DrawNode::create();
     for(int i=0;i<NUM_PUNTOS-1;i++) {
     drawNode->drawLine(Vec2(v[i].x * PTM_RATIO, v[i].y * PTM_RATIO), Vec2(v[i+1].x * PTM_RATIO, v[i+1].y * PTM_RATIO), Color4F(1.0, 0.5, 0.5, 1.0));
     drawNode->drawSolidRect(Vec2(448, 0), Vec2(480, 32), Color4F(0.5, 0.5, 0.5, 0.5));
     }
     node->addChild(drawNode);
    
    
    
    
    
    // Listener de la pantalla táctil para dar impulso a la bola
    
    // Descomenta para el apartado (e)
     
     m_listener = EventListenerTouchOneByOne::create();
     m_listener->setSwallowTouches(true);
     
     m_listener->onTouchBegan = [=](Touch* touch, Event* event) {
     
     Vec2 impulso = (m_bola->getNode()->getPosition() - node->convertTouchToNodeSpace(touch)) * 5;
     
     m_bola->getBody()->ApplyLinearImpulse(b2Vec2(impulso.x / PTM_RATIO,impulso.y / PTM_RATIO), m_bola->getBody()->GetPosition(), true);
     
     return true;
     };
     
     node->getEventDispatcher()->addEventListenerWithSceneGraphPriority(m_listener, m_bola->getNode());
    
    
    
    
    // Crea un sensor para la meta
    
    /* Descomenta para el apartado (f)
     
     b2FixtureDef goalFixtureDef;
     b2PolygonShape goalShape;
     goalShape.SetAsBox(16.0 / PTM_RATIO, 16.0 / PTM_RATIO, b2Vec2(464.0 / PTM_RATIO, 16.0 / PTM_RATIO), 0);
     goalFixtureDef.shape = &goalShape;
     goalFixtureDef.density = 1.0;
     goalFixtureDef.isSensor = true;
     m_fixture_goal = m_body->CreateFixture(&goalFixtureDef);
     
     m_world->SetContactListener(new GoalContactListener(m_fixture_goal));

     */

    return node;
    
}

b2World* Mundo::getPhysicsWorld() {
    return m_world;
}

Vector<Lata*> Mundo::getLatas(){
    return m_latas;
}

void Mundo::update(float delta){
    // Descomentar para el apartado (b)
     m_world->ClearForces();
     m_world->Step(1.0/60.0, 6, 2);
    
    m_bola->update(delta);
    
    for (auto lata: this->m_latas){
        lata->update(delta);
    }
}

// Funciones del listener para detectar el contacto con la meta

GoalContactListener::GoalContactListener(b2Fixture *fixture) {
    m_fixture = fixture;
}

void GoalContactListener::BeginContact(b2Contact *contact) {
    if(contact->GetFixtureA()==m_fixture || contact->GetFixtureB()==m_fixture) {
        CCLOG("Entra en meta");
    }
}

void GoalContactListener::EndContact(b2Contact *contact) {
    if(contact->GetFixtureA()==m_fixture || contact->GetFixtureB()==m_fixture) {
        CCLOG("Sale de meta");
    }
}
