//
//  Player.h
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 14/1/15.
//
//

#pragma once

#include "../Engine2D/PhysicsGameEntity.h"

class Bola: public PhysicsGameEntity{
public:
    
    bool init();
    
    Node* getNode();
    
    CREATE_FUNC(Bola);
    
private:
    Sprite *m_bolaSprite;
};


