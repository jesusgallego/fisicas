//
//  MarioSc.h
//  Test01
//
//  Created by Fidel Aznar on 13/1/15.
//
//

#pragma once

#include "../Engine2D/SimpleGame.h"
#include "Bola.h"
#include "Lata.h"
#include "GUI.h"
#include "Mundo.h"

class Game: public SimpleGame{
    
public:
    
    bool init();
        ~Game();
    
    void preloadResources();
    void start();
    
    void updateEachFrame(float delta);
    
    // implement the "static create()" method manually
    CREATE_FUNC(Game);
    
private:
    Bola *m_bola;
    GUI *m_gui;
    Mundo *m_mundo;

    
};


