//
//  Npc.cpp
//  VideojuegosCocos
//
//  Created by Fidel Aznar on 16/1/15.
//
//

#include "Lata.h"
#include "Mundo.h"

bool Lata::init(){
    PhysicsGameEntity::init();

    return true;
}

Node* Lata::getNode(){
    
    if(m_node==NULL) {
 
        // Crea nodo gráfico
        
        m_sprite = Sprite::createWithSpriteFrameName("lata.png");
        m_node = m_sprite;
        m_node->setLocalZOrder(0);
        
        // Crea cuerpo físico
        b2World *world = Mundo::getInstance()->getPhysicsWorld();
        
        b2BodyDef bodyDef;
        bodyDef.type = b2BodyType::b2_dynamicBody;
        
        b2FixtureDef fixtureDef;
        b2PolygonShape shape;
        shape.SetAsBox(0.5 * m_sprite->getContentSize().width / PTM_RATIO, 0.5 * m_sprite->getContentSize().height / PTM_RATIO);
        fixtureDef.shape = &shape;
        fixtureDef.density = 1.0;
        fixtureDef.restitution = 0.0;
        
        m_body = world->CreateBody(&bodyDef);
        m_body->CreateFixture(&fixtureDef);
    }
    
    return m_node;
}



